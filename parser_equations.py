import string

operator_stoppers=("!","^","*","-","+","/","<",">","=","(",")","[","]","\\"," ","\t",\
                   "1","2","3","4","5","6","7","8","9","0")
trivial_operators=("!","^","*","-","+","/","<",">","=")
digits=("1","2","3","4","5","6","7","8","9","0")
variables=tuple(string.ascii_letters)
parenthesis=("(",")","[","]")

class Equation:
    
    def __init__(self,eq_string):
        self.equation_string=eq_string
        self.special_operators_number=0
        self.trivial_operators_number=0
        self.digits_number=0
        self.variables_number=0
        self._parser(eq_string)

    def _skip_operator_name(self, i, eq_string, n):
        it=i+1
        while it<n and eq_string[it] not in operator_stoppers:
            it+=1
        return it
            
    def _parser(self, eq_string):
        n=len(eq_string)
        i=0
        while i<n:
            symbol=eq_string[i]
            if symbol in trivial_operators:
                self.trivial_operators_number+=1
            elif symbol in digits:
                self.digits_number+=1
            elif symbol in variables:
                self.variables_number+=1
            elif symbol==" " or symbol=="\t":
                continue
            elif symbol=="\\":
                self.special_operators_number+=1
                i=self._skip_operator_name(i, eq_string, n)
            elif symbol in parenthesis:
                i+=0 # do nothing
            else:
                print("CASE NOT CONSIDERED!!!!!!!!!! ", symbol)
            i+=1

    def get_numbers(self):
        return self.trivial_operators_number, self.digits_number, self.variables_number, self.special_operators_number
    
s="\sin(x+y(3+z)^3)"
eq=Equation(s)
print(eq.get_numbers())
